package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    //定义一个全局的记录器，通过LoggerFactory获取
    private static final Logger logger=LoggerFactory.getLogger(DemoController.class);
    @Autowired
    private UserMapper userMapper;

    @GetMapping("getMsg")
    public User getMsg(){
        User user=userMapper.getByAccount("admin");
        logger.debug("用户=={}"+user.toString());
        return user;
    }
}
